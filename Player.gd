extends KinematicBody2D

var input = Vector2.ZERO
var velocity = Vector2.ZERO

var run_speed = 500
var gravity = 10

var state = 1

func _ready():
	pass
	
func _physics_process(_delta):
	input = Input.get_vector("left", "right", "up", "down")
	
	match state:
		0:
			if input.x == 0:
				velocity.x = lerp(velocity.x, 0, 0.4)
			else:
				velocity.x = sign(input.x) * run_speed
				velocity.x = clamp(velocity.x, -run_speed, run_speed)
				
			move_and_slide(velocity, Vector2( 0 , -1 ))
			if !is_on_floor():
				state = 1
				print_debug("air")
		1:
			velocity.y += 10
			
			move_and_slide(velocity, Vector2( 0 , -1 ))
			if is_on_floor():
				state = 0
				print_debug("ground")

